# Apprendre l'Injection de dépendance

J'utilise la vidéo "PHP - Injection de Dépendance - Container - PSR-11" de Thomas Boileau : https://www.youtube.com/watch?v=cHt0xQ_6MiA .

Là je revois la vidéo en x1.5 pour mieux prendre des notes.

# Pourquoi ce Design Pattern ?

Lorsqu'on a des **classes** ou **méthodes** qui ont besoin d'utiliser des objets / classes.
Mais si on utilise par exemple de la classe **DatabaseMySql** mais que demain on veut utiliser la classe **DatabaseOracle** alors on aura un problème de dépendance.
On parle alors d'**injection de dépendance**.

Un des exemples courant quand on code une fonctionnalité qui dépend d'une base de données :

```php
<?php

namespace App;

use DB\DatabaseMysql;

class Foo
{
    // code

    public function doSomething(){
        $mysql = new DatabaseMysql();
    }

    // code
}
```

Et voici la base de données :

```php
<?php

namespace DB;

class DatabaseMysql
{

}
```

Là on a un problème. On instancie une classe dans une méthode et on a déjà ce genre de cas dans d'autres classes autre que `Foo()` et dans d'autres méthodes en plus de `doSomething()`. Lorsqu'on voudra changer de SGBD, on va devoir changer le nom de la classe et de ses arguments dans X **classes** et dans X **méthodes**.

De plus, à chaque appelle de la méthode `doSomething()` on crée une nouvelle instance donc on occupe encore plus de mémoire.

Ok, mais quelle est donc la solution ?

La solution est d'utiliser le *design pattern* injection de dépendance qui permet : 

1. De ne pas dupliquer la même instance ;
2. De ne pas se répéter dans dans les méthodes et classes.

Basiquement, on va mettre notre *database* dans le constructeur de la classe. Par exemple :

```php
<?php

namespace App;

use DB\DatabaseMysql;

class Foo
{
    public function __product(DatabaseMysql $db){
        $database = $db;
    }
    
    // code

    public function doSomething(){
        $db->select('*')->from('foo');
    }

    // code
}
```

Pour utiliser la même instance on doit utiliser un *container*.

# Qu'est-ce qu'un `container` ?

Un `container` est une "boite" qui contient les **instances** de nos **services**.
Un service peut être : Une *database*, un *router*, une classe *Client*, une classe *CompteClient*, etc.

Par exemple, ces services ou **instances** peuvent être stockés dans notre variable `$instances`. 
Cette variable est de type *array* associatif / **json**.

On représente notre instance à l'aide de la variable `$id`.
`$id` est de type `string` et contient le **nom de notre Classe**.
Pour trouver cette classe dans `$instances` on lui mais comme clée l'id. Soit `$instances[$id]`.

Un *container* est **standard** dans le **PSR-11** et possède l'interface `ContainerInterface()`.

Exemple :

30min

```php
<?php

//code

class Container implements ContainerInterface
{
    private array $instance = [];

    public function get($id)
    {
        if(!this->has($id)){
            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

    public function has($id)
    {
        return isset($this->instances[$id]);
    }
}   
```

# Méthodes de base de l'interface `ContainerInterface()`

Dans l'interface `ContainerInterface()`. Nous avons 2 méthodes qu'on doit inclure : `get()` et `has()`.

- `get()` : Doit retourner notre instance depuis notre variable `instances` soit `$instances[$id]`. On check l'existance de cette instance avec `has()` sinon on crée une nouvelle ;


- `has()` : Doit retourner un *boolean* et check si notre instance est déjà instanciée.

> Mais qu'est-ce qu'il se passe si notre classe qu'on souhaite utiliser possède des dépendances comme d'autres classes (arguments) ?

Là on doit utiliser `reflectionClass()`.

# Utiliser `reflectionClass()`

Dans le cas où on veut utiliser la classe `DatabaseMySql` mais qui possède elle même des dépendances comme suit :

```php
<?php

namespace DB;

class DatabaseMysql
{
    public function __construct(Router $router){
        //code
    }

}
```

On aura une erreur car il faut récupérer ses arguments.

On va devoir utiliser `reflectionClass()` pour analyser le constructeur d'une classe dont on va utiliser dans notre container.

Cette classe nous apporte les informations concernant une classe en lui indiquant son nom.

Une fois qu'on a instancié un objet de `ReflectionClass` on va récupérer le constructeur de la classe avec la méthode `getConstructor()`. Mais si cette méthode nous renvoi `null` car une classe peut ne pas avoir de constructeur, et bah nous allons créer une nouvelle instance en utilisant la méthode `newInstance()`.

Dans le cas où il y a un constructeur, on va obtenir tous les arguments en utilisant la méthode `getParameters()`. Elle renvoie une liste de paramètre où peut boucler dessus dont la variable "unitaire" sera `parameter`.

Okay, si ce `parameter` est une classe ? On utilise la méthode `getClass()` (`$parameter->getClass()`) pour obtenir le nom de la classe associé à son `namespace`.
Nous on veut juste le nom. Alos on remet par dessus un `getName()` pour obtenir son nom `$parameter->getClass()`.
Puis, on va faire un appel de la méthode `get()` pour créer le service de l'argument de la classe `Database` et on va la stockée dans la nouvelle variable `$instance`.

Voici le code pour cette représentation :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    private array $instance = [];

    public function get($id)
    {
        if(!this->has($id)){
            $reflectionClass = new ReflectionClass($id);

            $constructor = $reflectionClass->getConstructor();

            if(null === $constructor){
                $this->instances[$id] = $reflectionClass->newInstance();
            }
            else{
                $parameters = $constructor->getParameters();

                foreach($paramaters as $parameter){
                    $instance = $this->get($parameter->getClass()->getName());
                }
            }

            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

    public function has($id)
    {
        return isset($this->instances[$id]);
    }
}   
```

Okay, mais si on veut être plus poussé ?

On peut instancier les arguments avec la méthode `newInstanceArgs()` au lieu de `newInstance()`.
Au lieu d'utiliser le *foreach* de la ligne **XX** on va utiliser un `array_map()` qui va transformer les valeurs d'un tableau en autre forme.
Voici un exemple concrêt :

```php
<?php
    $array = [1, 2, 3, 4, 5];

    $newArray = array_map(fn (int $nb) => $nb * 2, $array); // Output: [2, 4, 6, 8, 10]
```

Ça va transformer notre tableau `[1,2,3,4,5]` en `[2,4,3,8,10]`. À noter que `fn` est la **fonction flêchée** apporté par PHP7.4.
D'ailleurs, il faut éviter les `array_map()' sur des tableaux longs car le temps de traitement sera plus long.

Maintenant, revenons à notre code.
Dans notre *array_map* on va mettre ce code `fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()), $parameters` . Soit une itération de `$parameters` en `$parameter` dont la nouvelle valeur dans cette itération sera une nouvelle instanciation avec `$this->get($parameter->getClass()->getName())`.

Voici le code fonctionnel de notre explication :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    private array $instance = [];

    public function get($id)
    {
        if(!this->has($id)){
            $reflectionClass = new ReflectionClass($id);

            $constructor = $reflectionClass->getConstructor();

            if(null === $constructor){
                $this->instances[$id] = $reflectionClass->newInstance();
            }
            else{
                $parameters = $constructor->getParameters();
    
                $this->instances[$id] = $reflectionClass->newInstanceArgs(
                    array_map(
                        fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                        $parameters
                    )
                );
            }

            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

    public function has($id)
    {
        return isset($this->instances[$id]);
    }
}   
```

Maintenant, on va utiliser un principe SOLID, le **D** pour être exact !

# Utiliser les interfaces comme argument

Dans les principes SOLID, on va utiliser le **D** qui est le *Dependency Inversion Principle*.

Concrêtement, ça veut dire qu'**on ne doit pas dépende d'une implémentation mais d'une abstraction**.

Alors, que veut dire "on ne doit pas dépende d'une implémentation [...]" ?

Si on reprend le code de la classe `Database` :

```php
<?php

namespace DB;

class DatabaseMysql
{
    public function __construct(Router $router){
        //code
    }

}
```

On voit que dans le constructeur on a définit `Router $router`.
Là **on est dépendant** de cette classe `Router` et ça veut dire qu'il y a un logique métier de code et on ne peut pas changer de classe sans trop modifier le code facilement. On doit trouver une abstraction.

Du coup, que veut dire **abstraction** ?

Tout simplement l'utilisation d'une `interface`. Dans cette interface on a pas d'implémentation mais la définition d'un comportement.

Par exemple, on va définir notre interface de la classe `Router` :

```php
<?php

namespace Interfaces;

interface RouterInterface
{
    public function call();
}
```

Voici le code de notre classe `Router` avec l'implémentation de l'interface :

```php
<?php

namespace Routes;

class Router implements RouterInterface
{
    public function __construct(Bar $bar)
    {

    }

    public function call()
    {
        // TODO: Implement call() method.
    }
}
```

Maintenant dans notre code de `Database` on va changer le type de notre `$router`.

```php
<?php

namespace DB;

class DatabaseMysql
{
    public function __construct(RouterInterface $router){
        //code
    }

}
```

Mais pourquoi `RouterInterface` ?

Parce qu'on s'enfout du comportement réel de router, on veut juste connaître son comportement qui est la méthod `call()`.

Mais là ça ne va pas marcher car on ne peut pas **instancier des interfaces**. On va donc devoir utiliser des **Aliases** pour dire que telle interface sera utilisée par telle classe.

# Les aliases

Le but d'un alias c'est que dès qu'on récupère telle interface ça pointe vers telle classe.

On va déclarer cet alias dans notre **container**.

On essaye d'avoir quelque chose qui ressemble à ça :

```php
<?php
    private array $aliases = [
        RouterInterface::class => Router::class
    ];
```


Okay, pour ça on va dans un premier temps créer notre variable privée `$aliases` (`private array $aliases`) puis de créer une méthode qui va nous ajouter notre alias dans la variable `$aliases`.
Code dans container :


```php
<?php
    public function addAlias(string $id, string $class):self
    {
        //code
        $this->aliases[$id] = $class;

        return $this;
    }
```

Le truc c'est lorsqu'on va utiliser la méthode `get()` dans notre `Container` il faut qu'on sache si oui ou non c'est une interface pour pouvoir l'ajouter dans la liste `$aliases`.
On va utiliser la méthode `isInterface()` de `ReflectionClass` :

```php
<?php
    // code de container

        public function get($id)
    {
        if(!this->has($id)){
            $reflectionClass = new ReflectionClass($id);

            if( $reflectionClass->isInterface() ){
                return $this->get($this->aliases[$id]);
            }

            $constructor = $reflectionClass->getConstructor();

            if(null === $constructor){
                $this->instances[$id] = $reflectionClass->newInstance();
            }
            else{
                $parameters = $constructor->getParameters();
    
                $this->instances[$id] = $reflectionClass->newInstanceArgs(
                    array_map(
                        fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                        $parameters
                    )
                );
            }

            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

// code de container
```



```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    private array $instance = [];

    private array $aliases = [];


    public function get($id)
    {
        if(!this->has($id)){
            $reflectionClass = new ReflectionClass($id);

       
            if( $reflectionClass->isInterface() ){
                return $this->get($this->aliases[$id]);
            }
   
           $constructor = $reflectionClass->getConstructor();

            if(null === $constructor){
                $this->instances[$id] = $reflectionClass->newInstance();
            }
            else{
                $parameters = $constructor->getParameters();
    
                $this->instances[$id] = $reflectionClass->newInstanceArgs(
                    array_map(
                        fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                        $parameters
                    )
                );
            }

            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

    public function has($id)
    {
        return isset($this->instances[$id]);
    }

    public function addAlias(string $id, string $class):self
    {
        $this->aliases[$id] = $class;

        return $this;
        }

} 
```

Dans une alias, **une seule interface** pointe sur **une classe**. Mais **une classe** peut être pointé par **plusieurs interfaces**.

Donc une classe peut avoir **plusieurs aliases**.
Mais **une alias** pointe que sur **une seule classe**.

Est-ce qu'on peut aller plus loin ?

Imaginons que le service qu'on souhaite avoir n'a pas la notion de *shared*.
Le *shared* veut dire qu'un service n'est pas partagé signifie qu'on doit **recréer une nouvelle instance**.

Par exemple avec un TDD :

```php
<?php

class ContainerTest extends TestCase
{
    public function test()
    {
        $container = new Container();

        // Code
        
        $foo1 = $container->get(Foo::class);
        $foo2 = $container->get(Foo::class);
        $this->assertNotEquals(spl_object_id($foo1), spl_object_id($foo2)); // output : Failed asserting that 560 is not equal to 460.
    }
}
```

Ce code de test montre que `$foo1` et `$foo2` est la même instance donc le même ID.
Donc, comment faire ? C'est là qu'intervient la notion de `shared`.

Mais on va devoir utiliser la notion de `Definition`. 

# Qu'est-ce qu'une `Definition` ?

Une **Definition** c'est le parémétrage d'un service.
On va créer une nouvelle classe qui est `Definition`.

```php
<?php

namespace App;

class Definition
{
    private string $id;

    private bool $shared = true;

    private array $aliases = [];

    private array $dependencies = [];

    public function __construct(string $id, bool $shared = true, array $aliases = [], array $dependencies = [])
    {
        $this->id = $id;
        $this->shared = $shared;
        $this->aliases = $aliases;
        $this->dependencies = $dependencies;
    }
}
```

Donc notre définition a comme propriété l'ID de notre classe (le nom de classe), savoir si c'est partagé ou non avec *shared*, la liste des aliases et les dépendances de notre classe (arguments du constructeur).

On va modifier notre **Container** pour créer un tableau `$definitions`, la méthode `getDefinition()` et `register()` :

<!-- **Attention** : J'en suis à 1h19min. Je dois continuer après. -->

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{

    //code

    private array $definitions =[];

    public function register(string $id):self
    {

        $reflectionClass = new ReflectionClass($id);

        if($reflectionClass->isInterface()){
            $this->register($this->aliases[$id]);
            $this->definitions[$id] = &$this->definitions[$this->alias[$id]];
            return $this;
        }
        
        $dependencies = [];

        if(null !== $reflectionClass->getConstructor())
        {
            $dependencies = array_map(
                    fn (  ReflectionParameter $parameter ) => $this->getDefinition($parameter->getClass()->getName())),
                    $reflectionClass->getConstuctor()->getParameters()
            );
        }

        $aliases = array_filter($this->aliases, fn( (string $alias) => $id === $alias ));

        $definition = new Definition($id, true, $aliases, $dependencies);

        $this->definitions[$id] = $definition; // push

        return $this;
    }

    public function getDefinition($id): Definition
    {
        if(!isset($this->definitions[$id])){
            $this->register($id);
        }

        return $this->definitions[$id];
    }

    //code

    public function get($id)
    {
        if(!this->has($id)){
            $reflectionClass = new ReflectionClass($id);


            if( $reflectionClass->isInterface() ){
                return $this->get($this->aliases[$id]);
            }

            $constructor = $reflectionClass->getConstructor();

            $this->register($id);

            if(null === $constructor){
                $this->instances[$id] = $reflectionClass->newInstance();
            }
            else{
                $parameters = $constructor->getParameters();
    
                $this->instances[$id] = $reflectionClass->newInstanceArgs(
                    array_map(
                        fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                        $parameters
                    )
                );

                $this->register($id);
            }

            $this->instances[$id] = new $id();
        }

        return $this->instances[$id];
    }

    // code

}
```

Qu'est-ce qu'il faut comprendre ?

Déjà qu'on a créé notre variable `$definitions` en *private* et stock nos différentes définitions.

On a la méthode `getDefinition()` qui permet de renvoyer la **Définition d'une classe** si elle est déjà enregistrée. Sinon on l'enregistre à l'aide de la méthode `register()`.

On a la méthode `register()` qui permet de renvoyer la référence du même objet (avec `return $this;`).
On a 4 instructions en dehors du *return* :

- On Crée un objet de la classe `reflectionClass` ;
- On check si c'est une interface et si c'est le cas, on le `register()`. Puis, on ajoute dans `$definitions[$id]` la définition de l'alias avec `&$this->$definitions[$this->alias[$id]]`. Le `&` permet d'obtenir la référence. Puis on *return* l'objet ;
- On crée une variable `$dependencies` dont la valeur est un *array* à vide ;
- Si le *constructor* n'est pas vide, alors on récupère la liste des dépendances pour l'ajouter dans les `$dependencies` ; 
- On cherche tous les aliases enregistrées pour l'ajouter dans `$aliases` ;
- On crée une nouvelle définition stocké dans `$definition` en précisant le nom de la classe (`$id`), bien sûr, mais aussi la valeur à `true` à `shared` pour qu'on puisse la réutilisés, les aliases et aussi avec la liste des dépendances dans la variable `$dependencies`;
- On *push* cette nouvelle définition dans `$definitions[$id]` ;
- Enfin, on retourne l'objet `$definitions` avec `return $this;` .

Puis dans la méthode `get()` on appelle la méthode `register()`.

Là où c'est gênant c'est qu'on ne peut pas changer l'état `shared`.
On va donc créer un *setter* de `shared` dans notre classe `Definition` (Definition.php) :

```php
<?php

namespace App;

class Definition
{
    //code
    public function setShared(bool $shared): void
    {
        $this->shared = $shared;
    }
}
```

Même avec ce code on aura toujours le même bug qui est que `$foo1` et `$foo2` sont toujours les mêmes (voir la classe `ContainerTest`).

Pour plus de clareté et alléger notre méthode `get()` on va déplacer une partie du code dans une nouvelle méthode qui sera `resolve()` :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{

    //code
    private function resolve($id): object
    {
        $reflectionClass = new ReflectionClass($id);


        if( $reflectionClass->isInterface() ){
            return $this->resolve($this->aliases[$id]);
        }

        $constructor = $reflectionClass->getConstructor();

        $definition = $this->getDefinition($id);

        if(null === $constructor){
            return $reflectionClass->newInstance();
        }
        $parameters = $constructor->getParameters();

        return $reflectionClass->newInstanceArgs(
            array_map(
                fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                $parameters
            )
        );
    }
}
```

Puis, dans la méthode `get()` nous aurons cette modification :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    //code

    public function get($id)
    {
        if( !$this->has($id) ){
            $this->instances[$id] = $this->resolve($id);
        }

        return $this->instances[$id];
    }
```

Okay, maintenant on va créer notre *getter* pour `shared` pour l'utiliser par la suite dans la méthode `get()` :

```php
<?php

namespace App;

class Definition
{
    //code
    public function isShared(bool $shared): bool
    {
        return $this->shared;
    }
}
```

Voici le code de la méthode `get()` :


```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    //code

    public function get($id)
    {
        if( !$this->has($id) ){
            $instance = $this->resolve($id);

            if($this->getDefinition($id)->isShared()){
                return $instance;
            }

            return $this->instances[$id] = $instance;
        }

        return $this->instances[$id];
    }
```

Maintenant, si on on modifie notre test :

```php
<?php

class ContainerTest extends TestCase
{
    public function test()
    {
        $container = new Container();

        $container->getDefinition(Foo::class)->setShared(false);

        // Code
        
        $foo1 = $container->get(Foo::class);
        $foo2 = $container->get(Foo::class);
        $this->assertNotEquals(spl_object_id($foo1), spl_object_id($foo2)); // output : OK
    }
}
```

Notre test est valide et nous avons bien `Foo` qui n'est pas `shared` donc on peut créer autant d'instance de `Foo` qu'on le souhaite !

Mais il y a moyen de mieux écrire notre méthode `resolve()` mais on aura besoin de définir une méthode getter `getId()` dans notre classe `Definition` et lui ajouter une nouvelle propriétée qui est `$class` et qui aura pour objectif de contenir une instance de `ReflectionClass` avec *getter*.


```php
<?php

namespace App;

//code
use ReflectionClass;

class Definition
{
    // code

    private string $id;

    private bool $shared = true;

    private array $aliases = [];

    private array $dependencies = [];

    private ReflectionClass $class;

    public function __construct(string $id, bool $shared = true, array $aliases = [], array $dependencies = [])
    {
        $this->id = $id;
        $this->shared = $shared;
        $this->aliases = $aliases;
        $this->dependencies = $dependencies;
        $this->class = new ReflectionClass($id);
    }

    // code

    public function getId():string
    {
        return $this->id;
    }

    public function getClass(): ReflectionClass
    {
        return $this->class;
    }
}
```

Notre méthode `resolve()` :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{

    //code
    private function resolve($id): object
    {
        $definition = $this->getDefinition($id);

        if( $definition->getClass()->isInterface() ){
            return $this->resolve($definition->getId());
        }

        $constructor = $definition->getClass()->getConstructor();

        if(null === $constructor){
            return $definition->getClass()->newInstance();
        }
        $parameters = $constructor->getParameters();

        return $definition->getClass()->newInstanceArgs(
            array_map(
                fn (  ReflectionParameter $parameter ) => $this->get($parameter->getClass()->getName()),
                $parameters
            )
        );
    }
}
```

Pour le coup, graçe à `resolve()` on utilise l'instance `$definition` pour "manipuler" nos classes.

On peut faire encore mieux en déplaçant une partie du code de `resolve()` dans une méthode qui est `newInstance()` et qui renvoie un objet depuis `Definition`.

```php
<?php

// code

use ReflectionClass;
use ReflectionParameter;

//code

class Definition
{
    // code
    
    public function newInstance(ContainerInterface $container): self
    {
        $constructor = $this->class->getConstructor();

        if(null === $constructor){
            return $this->class->newInstance();
        }
        $parameters = $constructor->getParameters();

        return $this->class->newInstanceArgs(
            array_map(
                fn (  ReflectionParameter $parameter ) => $container->get($parameter->getName()),
                $parameters
            )
        );
    }
        
    // code
}
```

Voici la mise à jour de la méthode `resolve()` :

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{

    //code
    private function resolve($id): object
    {
        $definition = $this->getDefinition($id);

        if( $definition->getClass()->isInterface() ){
            return $this->resolve($definition->getId());
        }

        return $definition->newInstance($this);
    }
}
```

Là encore fonctionne mais on peut encore optimiser voir même ne plus avoir besoin de *resolve* !

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    //code

    public function get($id)
    {
        if( !$this->has($id) ){
            $instance = $this->getDefinition($id)->newInstance($this);

            if($this->getDefinition($id)->isShared()){
                return $instance;
            }

            return $this->instances[$id] = $instance;
        }

        return $this->instances[$id];
    }
```

Bien sûr, on aurait pas besoin de la méthode `getClass()` et la propriété `$id` dans la classe `Definition`.

# La gestion des `parameters`

Il nous manque de voir la gestion des paramètres.
Par exemple dans la classe `DatabaseMysql` on a les paramètres suivants :

```php
<?php

namespace DB;

class DatabaseMysql
{
    public function __construct(string  $dbUrl, string $dbName, string $dbUser, string $dbPassword){
        //code
    }

}
```

Dans ce cas là on aura une nouvelle propriétée dans *Container* qui sera *parameter* de type *array*.


```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;

class Container implements ContainerInterface
{
    // code

    private array $parameters = [];

    //code
```

Si on relance nos tests on aura un problème car nos paramètres de la classe `DatabaseMysql` est de type scalaire (integer, boolean, char, "string" (?), etc.) et non une **interface**.

Pour ça on va changer le code de la méthode `register()` de la classe `Container()`.

```php
<?php

//code

class Container implements ContainerInterface
{

    //code

    public function register(string $id):self
    {

        $reflectionClass = new ReflectionClass($id);

        if($reflectionClass->isInterface()){
            $this->register($this->aliases[$id]);
            $this->definitions[$id] = &$this->definitions[$this->alias[$id]];
            return $this;
        }
        
        $dependencies = [];

        if(null !== $reflectionClass->getConstructor())
        {
            $dependencies = array_map(
                    fn (  ReflectionParameter $parameter ) => $this->getDefinition($parameter->getClass()->getName())),
                    array_filter( 
                        $reflectionClass->getConstuctor()->getParameters(),
                        fn(ReflectionParameter $parameter) => $parameter->getClass()
                    )
            );
        }

        $aliases = array_filter($this->aliases, fn( (string $alias) => $id === $alias ));

        $definition = new Definition($id, true, $aliases, $dependencies);

        $this->definitions[$id] = $definition; // push

        return $this;
    }

    // Code
}
```

Il faudra modifier le code de notre classe pour gérer les *parameters*.

```php
<?php

// code

class Container implements ContainerInterface
{
    // code
    
    public function addParameter($id, $value):self
    {
        $this->parameters[$id] = $value;

        return $this;
    }

    public function getParameter($id)
    {
        return $this->parameters[$id];
    }
    
    // code
}
```

Okay, notre *container* peut *push* ou *get* la propriété **parameters**.

Mais on doit aussi les gérer dans méthode `newInstance` de la classe `Definition`.

```php
<?php

// code

use ReflectionClass;
use ReflectionParameter;

//code

class Definition
{
    // code
    
    public function newInstance(ContainerInterface $container): self
    {
        $constructor = $this->class->getConstructor();

        if(null === $constructor){
            return $this->class->newInstance();
        }
        $parameters = $constructor->getParameters();

        return $this->class->newInstanceArgs(
            array_map(
                function (ReflectionParameter $parameter) use ($container){
                    
                    if($param->getClass() === null)
                    {
                        $container->
                    }

                    return $container->get($parameter->getName()),
                },
                $parameters
            )
        );
    }
        
    // code
}
```

Là on aura un problème au niveau de la ligne **XX** concernant ce code `$container->`.
En effet on ne peut atteindre que les méthodes `get()` et `has()`.

La solution serait de créer notre propre interface `ContainerInterface` qui est une interface enfant de `ContainerInterface` de **PSR**.

```php
<?php

namespace zak39\DependencyInjection;

use PSR\Container\ContainerInterface as PsrContainerInterface;

interface ContainerInterface extends PsrContainerInterface
{
    public function register(string $id): self;

    public function getDefinition(string $id): Definition;

    public function addParameter(string $id, int $value): self;

    public function getParameter(string $id);

}
```

Puis on supprime le `use PSR\Container\ContainerInterface;` dans la classe `Container` et de `Definition` afin qu'ils utilisent `ContainerInterface` à leur racine.
Maintenant nous pouvons continuer la modification de notre `register()`.

```php
<?php

// code

use ReflectionClass;
use ReflectionParameter;

//code

class Definition
{
    // code
    
    public function newInstance(ContainerInterface $container): self
    {
        $constructor = $this->class->getConstructor();

        if(null === $constructor){
            return $this->class->newInstance();
        }
        $parameters = $constructor->getParameters();

        return $this->class->newInstanceArgs(
            array_map(
                function (ReflectionParameter $parameter) use ($container){
                    
                    if($param->getClass() === null)
                    {
                        return $container->getParameter($param->getName());
                    }

                    return $container->get($parameter->getName()),
                },
                $parameters
            )
        );
    }
        
    // code
}
```

Voici un exemple de code pour l'exemple d'utilisation :

```php
<?php

class ContainerTest extends TestCase
{
    public function test()
    {
        $container = new Container();

        // code

        $container
            ->addParameter("dbUrl", "url");
            ->addParameter("dbName", "Name");
            ->addParameter("dbUser", "user");
            ->addParameter("dbPassword", "password");
        
        //code
    }
}
```

# Implémentation d'`NotFoundException`

On va implémenter l'**Exception** `NotFoundException`.

```php
<?php

namespace zak39\DependncyInjection;

class NotFoundException extends Exception implements NotFoundExceptionInterface
{

}
```

Cette exception pourra être utilisée dans nos  méthodes `get()` et `getParameter()` de la classe `Container`.

```php
<?php

//code

use ReflectionClass;
use ReflectionParameter;
use namespace zak39\NotFoundException;

class Container implements ContainerInterface
{

    // code

    public function getParameter($id)
    {
        if(!isset($this->parameters[$id]))
        {
            throw new NotFoundException();
        }
        return $this->parameters[$id];
    }
    
    //code

    public function get($id)
    {
        if( !$this->has($id) ){

            if(!class_exists($id) && !interface_exists($id)){
                throw new NotFoundException();
            }

            $instance = $this->getDefinition($id)->newInstance($this);

            if($this->getDefinition($id)->isShared()){
                return $instance;
            }

            return $this->instances[$id] = $instance;
        }

        return $this->instances[$id];
    }
```

Voici un test fonctionnel notre code `ContainerTest` :

```php
<?php

class ContainerTest extends TestCase
{
    public function test()
    {

        $container = new Container();

        //code
        $this->expectException(NotFoundExceptionInterface::class);
        $container->get(Bar::class);

    }

    public function testIfParameterNotFound()
    {
        $container = new Container();
        $this->expectException(NotFoundExceptionInterface::class);
        $container->getParameter(Bar::class);

    }

    public function testIfClassNotFound()
    {
        $container = new Container();
        $this->expectException(NotFoundExceptionInterface::class);
        $container->get(Bar::class);
    }
}
```



# Sources d'aides

Grafikart sur Discord : https://discord.com/channels/85154866468487168/85154866468487168/809020494187331625 ;

Romain Lanz sur Discord (dans le même thread) : https://discord.com/channels/85154866468487168/85154866468487168/809040986230882356 ;

# Installation

```php
composer require tboileau/dependency-injection ^1.0
```

# Add alias

```php
<?php

$container->addAlias(FooInterface::class, Foo::class);
$foo = $container->get(FooInterface::class);
```

# Add factory

```php
<?php

$container->addFactory(Foo::class, FooFactory::class, "create");
$foo = $container->get(Foo::class);
```

# Source GitHub

- https://github.com/TBoileau/dependency-injection .
