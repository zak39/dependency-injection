<?php

namespace zak39\DependencyInjection\Tests\Fixtures;

require "vendor/autoload.php";

/**
 * Class Database
 * @package zak39\DependencyInjection\Tests\Fixtures
 */
class Database
{

    /** Dans le principe SOLID, si on a ce constructeur : 
     * 
     * `public function __construct(Router $router)`
     * 
     * On a un problème de *Dependeny Inversion Principle* (D de SOLID).
     * Car notre constructeur a un logique métier assez fort où il est nécessaire
     * d'avoir forcément un constructeur.
     * 
     * Pour régler ce problème d'implémentation de Router, 
     * on va mettre l'Interface RouterInterface qui est sont **abstraction**.
     * L'intérêt est qu'on s'en fout du comportement réel / implémentation de notre Router.
     * Tout ce qu'on veut savoir c'est sont comportement qui est d'avoir une méthode `call()`.
     * On s'en fout du code qui est à l'intérieur de `call()`. Parce qu'elle est dans notre interface.
     * 
     * Si je relance le test, voici ce que j'aurais comme erreur :
     * 
     * `Error: Cannot instantiate interface zak39\DependencyInjection\Tests\Fixtures\RouterInterface`
     * 
     * On nous dit qu'on ne peut pas instancier une interface.
     * Là on est embêté car si on veut respecter l'inversion des dépendances
     * Avec notre container de dépendance et bah on ne peut pas. Parce que là on ne gère pas les interfaces.
     * Qu'est-ce qu'une interface ?
     * Ce sont des alias et le but c'est de gérer dans notre container de dire "Quand je veux récupérer
     * telle interface. Il faut que ça pointe, vers telle classe en question.
     * 
     * Or, ça ne peut pas se faire de manière automatique.
     * 
     * À noter : Symfony peut le faire car il a la notion d'autoconfiguration.
     * Nous on va faire de l' **auto-wirering** qui consiste à envoyer automatiquement
     * et dynamiquement l'instance qu'on souhaite avoir dans notre constructeur.
     * L'auto-configuration c'est qu'il fasse lui même la configuration de dire "Okay Interface pointe vers Router".
     * 
     * 
     */
    // public function __construct(RouterInterface $router)
    public function __construct(string $dbUrl, string $dbName, string  $dbUser, string $dbPassword)
    {
        // var_dump($dbUrl, $dbName, $dbUser, $dbPassword);
    }
    
}