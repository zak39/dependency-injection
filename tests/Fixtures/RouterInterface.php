<?php

namespace zak39\DependencyInjection\Tests\Fixtures;

require 'vendor/autoload.php';

/**
 * Interface RouterInterface
 * @package zak39\DependencyInjection\Tests\Fixtures
 */
interface RouterInterface
{
    public function call();
}