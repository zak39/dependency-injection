<?php

namespace zak39\DependencyInjection\Tests;

require "vendor/autoload.php";

use PHPUnit\Framework\TestCase;
use Psr\Container\NotFoundExceptionInterface;
use zak39\DependencyInjection\Container;
use zak39\DependencyInjection\Tests\Fixtures\Database;
use zak39\DependencyInjection\Tests\Fixtures\Router;
use zak39\DependencyInjection\Tests\Fixtures\Foo;
use zak39\DependencyInjection\Tests\Fixtures\RouterInterface;

/**
 * Class Container
 * @package zak39\DependencyInjection\Tests
 */

class ContainerTest extends TestCase
{
    public function test()
    {
        $container = new Container();

        // var_dump($container); // => for aliases #1
        $container->addAlias(RouterInterface::class, Router::class);

        $container->getDefinition(Foo::class)->setShared(false);

        $container
        ->addParameter("dbUrl", "url")
        ->addParameter("dbName", "name")
        ->addParameter("dbUser", "user")
        ->addParameter("dbPassword", "password");

        $database1 = $container->get(Database::class);  //La première fois, il va arriver sur le if et il va dire "ah non je ne l'ai pas, donc je crée une instance dans le tableau $instances.
        $database2 = $container->get(Database::class);  // La deuxième fois, est-ce que j'ai déjà cette instance ? Ah oui je l'ai et je vais directement dans le return.

        $this->assertInstanceOf(Database::class, $database1);
        $this->assertInstanceOf(Database::class, $database2);

        /**
         * Doit être egal car cela doit être la même instance.
        */
        $this->assertEquals(spl_object_id($database1), spl_object_id($database2));

        $this->assertInstanceOf(Router::class, $container->get(Router::class));
        // var_dump($container); // => for aliases #2
        $this->assertInstanceOf(Router::class, $container->get(RouterInterface::class));
        // var_dump($container); // => for aliases #3
        /**
         * Résultat de sortie des 3 var_dumps ("for aliases #X" dont X est le nombre ) :
         * 
         * (100%)object(zak39\DependencyInjection\Container)#394 (2) {
         *   ["instances":"zak39\DependencyInjection\Container":private]=>
         *   array(0) {
         *   }
         *   ["aliases":"zak39\DependencyInjection\Container":private]=>
         *   array(0) {
         *   }
         * }
         * object(zak39\DependencyInjection\Container)#394 (2) {
         *   ["instances":"zak39\DependencyInjection\Container":private]=>
         *   array(0) {
         *   }
         *   ["aliases":"zak39\DependencyInjection\Container":private]=>
         *   array(1) {
         *     ["zak39\DependencyInjection\Tests\Fixtures\RouterInterface"]=>
         *     string(47) "zak39\DependencyInjection\Tests\Fixtures\Router"
         *   }
         * }
         * object(zak39\DependencyInjection\Container)#394 (2) {
         *   ["instances":"zak39\DependencyInjection\Container":private]=>
         *   array(2) {
         *     ["zak39\DependencyInjection\Tests\Fixtures\Foo"]=>
         *     object(zak39\DependencyInjection\Tests\Fixtures\Foo)#62 (0) {
         *     }
         *     ["zak39\DependencyInjection\Tests\Fixtures\Router"]=>
         *     object(zak39\DependencyInjection\Tests\Fixtures\Router)#60 (0) {
         *     }
         *   }
         *   ["aliases":"zak39\DependencyInjection\Container":private]=>
         *   array(1) {
         *     ["zak39\DependencyInjection\Tests\Fixtures\RouterInterface"]=>
         *     string(47) "zak39\DependencyInjection\Tests\Fixtures\Router"
         *   }
         * }
         */

        $router1 = $container->get(RouterInterface::class);  //La première fois, il va arriver sur le if et il va dire "ah non je ne l'ai pas, donc je crée une instance dans le tableau $instances.
        $router2 = $container->get(RouterInterface::class);  // La deuxième fois, est-ce que j'ai déjà cette instance ? Ah oui je l'ai et je vais directement dans le return.
        $this->assertEquals(spl_object_id($router1), spl_object_id($router2));

        $foo1 = $container->get(Foo::class);  //La première fois, il va arriver sur le if et il va dire "ah non je ne l'ai pas, donc je crée une instance dans le tableau $instances.
        $foo2 = $container->get(Foo::class);  // La deuxième fois, est-ce que j'ai déjà cette instance ? Ah oui je l'ai et je vais directement dans le return.
        $this->assertNotEquals(spl_object_id($foo1), spl_object_id($foo2));
    }

    public function testIfClassNotFound()
    {
        $container = new Container();
        
        $this->expectException(NotFoundExceptionInterface::class);
        $container->get(Bar::class);
    }


    public function testIfParameterNotFound()
    {
        $container = new Container();
        
        $this->expectException(NotFoundExceptionInterface::class);
        $container->getParameter("fail");
    }
}