<?php

namespace zak39\DependencyInjection;

use Exception;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class NotFoundException
 * @package zak39\DependencyInjection
 */
class NotFoundException extends Exception implements NotFoundExceptionInterface
{
    
}