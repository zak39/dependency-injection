<?php

namespace zak39\DependencyInjection;

// use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionParameter;

require "vendor/autoload.php";

/**
 * Class Definition
 * @package zak39\DependencyInjection
 */
class Definition
{
    /**
     * @var string
     */
    private string $id;

    /**
     * @var bool
     */
    private bool $shared = true;

    /**
     * ?string : veut dire que string peut être null.
     * 
     * @var string
     */
    private array $aliases = [];

    /**
     * @var Definition[]
     */
    private array $dependencies = [];

    /**
     * @var ReflectionClass
     */
    private ReflectionClass $class;

    /**
     * Definition constructor
     * @param string $id
     * @param bool $shared
     * @param array $aliases
     * @param array $dependencies
     */
    public function __construct(string $id, bool $shared = true, array $aliases = null, array $dependencies = [])
    {
        $this->id = $id;
        $this->shared = $shared;
        $this->aliases = $aliases;
        $this->dependencies = $dependencies;
        $this->class = new ReflectionClass($id);
    }

    // Plus besoin.
    // /**
    //  * @return string
    //  */
    // public function getId(): string
    // {
    //     return $this->id;
    // }

    /**
     * @param bool $shared
     * @return self
     */
    public function setShared(bool $shared):self
    {
        $this->shared = $shared;

        return $this;
    }

    /**
     * @return bool
     */
    public function isShared():bool
    {
        return $this->shared;
    }

    // Plus besoin.
    // /**
    //  * @return ReflectionClass
    //  */
    // public function getClass(): ReflectionClass
    // {
    //     return $this->class;
    // }

    /**
     * @param ContainerInterface $container
     * @return object
     */
    public function newInstance(ContainerInterface $container): object
    {
        $constructor = $this->class->getConstructor(); 

        if( null === $constructor ){
            return $this->class->newInstance(); // newInstance de reflectionClass crée une nouvelle instance
        }

        /**
         * resultat du var_dump :
         * (100%)array(1) {
         *  [0]=>
         *  object(ReflectionParameter)#58 (1) {
         *  ["name"]=>
         *  string(6) "router"
         *  }
         * }
         */
        // var_dump($constructor->getParameters());

        $parameters = $constructor->getParameters();

        return $this->class->newInstanceArgs(
            // https://www.php.net/manual/fr/function.array-map.php
            // array_map : Permet de transformer les valeurs d'un tableau en une autre forme.
            // Ne pas utiliser array_map sur des longs tableaux.
            // fn (fonction flêchée) : https://www.php.net/manual/fr/functions.arrow.php
            // Permet d'instancier une liste de paramètre qui sont des classes.
            // array_map( fn (ReflectionParameter $parameter) => $container->get($parameter->getClass()->getName()), $parameters )
            array_map( function (ReflectionParameter $param) use($container)
            { 
                if( $param->getClass() === null )
                {
                    return $container->getParameter($param->getName());
                }
                
                return  $container->get($param->getClass()->getName());
            } , $parameters )
        );


        // foreach($parameters as $parameter){
        //     /**
        //      * getClass nous renvoi le nom de la classe dont on a besoin.
        //      * Résultat du var_dump :
        //      * 1 / 1 (100%)object(ReflectionClass)#59 (1) {
        //      *  ["name"]=>
        //      *  string(47) "zak39\DependencyInjection\Tests\Fixtures\Router"
        //      * }
        //      */
        //     // var_dump($parameter->getClass());

        //     $instance = $this->get($parameter->getClass()->getName());

        //     /**
        //      * Dans $instance, on récupère une instance de router (ou autre).
        //      * Voici le résultat de var_dump :
        //      * 1 / 1 (100%)object(zak39\DependencyInjection\Tests\Fixtures\Router)#60 (0) {
        //      *  }
        //      */
        //     var_dump($instance);
        // }

    }

}