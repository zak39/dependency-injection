<?php

namespace zak39\DependencyInjection;

require "vendor/autoload.php";

// use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionParameter;

/**
 * Class Container
 * @package zak39\DependencyInjection
 */

 class Container implements ContainerInterface
 {
     /**
      * @var array
      */
     private array $parameters = [];

    /**
     * @var Definition[]
     */
    private array $definitions = [];

    /**
     * @var array
     */
    private array $instances = [];

    /**
     * @var array
     */
    private array $aliases = [];
    // RouterInterface::class => Router::class => faire la correspondance Interface->Classe.

    /**
     * @param $id
     * @return $this
     */
    public function register(string $id): self
    {

        $reflectionClass = new ReflectionClass($id);

        if( $reflectionClass->isInterface() )
        {
            $this->register($this->aliases[$id]);
            $this->definitions[$id] = &$this->definitions[$this->aliases[$id]]; // le "&" de "&$this->definitions[$this->aliases[$id]]" c'est la référence.
            return $this;
        }


        $dependencies = [];

        if( $reflectionClass->getConstructor() !== null ){
            
            $dependencies = array_map(
                fn (ReflectionParameter $parameter) => $this->getDefinition($parameter->getClass()->getName()),
                array_filter(   
                    $reflectionClass->getConstructor()->getParameters(),
                    fn (ReflectionParameter $parameter) => $parameter->getClass()
                )
            );
        }

        $aliases = array_filter($this->aliases, fn(string $alias) => $id === $alias);

        $this->definitions[$id] = new Definition($id, true, $aliases, $dependencies);

        return $this;
    }

    /**
     * @param $id
     * @return Definition
     */
    public function getDefinition($id): Definition
    {
        // Si la definition n'existe pas.
        if( !isset($this->definitions[$id]) )
        {
            $this->register($id);
        }

        return $this->definitions[$id];
    }

    /**
     * @param $id
     * @param $value
     * @return $this
     */
    public function addParameter($id, $value): self
    {
        $this->parameters[$id] = $value;
        return $this;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getParameter($id)
    {
        if(!isset($this->parameters[$id]))
        {
            throw new NotFoundException();
        }
        return $this->parameters[$id];
    }

    /**
     * @param string $id
     * @return mixed|object
     * @throws NotFoundException
     */
    public function get($id)
    {
        // Todo: Implement get() method.

        /**
         * Il ne va pas instancier $id mais la valeur qui est à l'interrieur.
         * Par exemple : Database::class renvoi le nom de la classe.
         * Decommenter le var_dump pour voir.
         * Le résultat serait : `zak39\DependencyInjection\Tests\Fixtures`
         */
        // var_dump($id);


        if( ! $this->has($id) )
        {
            if(!class_exists($id) && !interface_exists($id))
            {
                throw new NotFoundException();
            }
            $instance = $this->getDefinition($id)->newInstance($this); // pour la définition.

            if( !$this->getDefinition($id)->isShared() )
            {
                return $instance;
            }

            $this->instances[$id] = $instance;
        }

        return $this->instances[$id];
    }

    // Plus besoin
    // /**
    //  * @param $id
    //  * @return object
    //  * @throws \ReflectionExcption
    //  */
    // private function resolve($id):object
    // {

    //     // Todo: Implement get() method.

    //     /**
    //      * Il ne va pas instancier $id mais la valeur qui est à l'interrieur.
    //      * Par exemple : Database::class renvoi le nom de la classe.
    //      * Decommenter le var_dump pour voir.
    //      * Le résultat serait : `zak39\DependencyInjection\Tests\Fixtures`
    //      */
    //     // var_dump($id);


    //     /**
    //      * obtient le constructor sous cette forme avec un var_dump :
    //      * 1 (100%)object(ReflectionMethod)#388 (2) {
    //      *  ["name"]=>
    //      *  string(11) "__construct"
    //      *  ["class"]=>
    //      *  string(49) "zak39\DependencyInjection\Tests\Fixtures\Database"
    //      *  }
    //      * 
    //      * S'il n'y a pas de __construc alos la valeur sera `NULL`
    //      */
    //     // var_dump($reflectionClass->getConstructor());


    //     /** 
    //      * Pour voir l'effet de notre `        $container->addAlias(RouterInterface::class, Router::class);`
    //      * Mais on a cette erreur :
    //      * `Error: Cannot instantiate interface zak39\DependencyInjection\Tests\Fixtures\RouterInterface`.
    //      * Ce qui est normal car une interface ne peut pas être instanciée.
    //      * À savoir, une interface peut avoir un constructeur mais ça ne change rien ici.
    //      * 
    //     */
    //     // var_dump($constructor);

    //     $definition = $this->getDefinition($id)->newInstance($this); // pour la définition.

    //     // if( $definition->getClass()->isInterface() ){
    //     //     // $id = $this->aliases[$id]; // Là l'`id` c'est le nom de mon interface.
    //     //     return $this->resolve($definition->getId()); // On retourne une instance non pas d'une interface mais de la classe qui est associée à l'interface.
    //     // }

    //     return $definition->newInstance($this);
    // }

    /**
     * @param string $id
     * @return bool
     */
    public function has($id)
    {
        // Todo: Implement has() method.
        return isset($this->instances[$id]);
    }

    // id de mon service.
    // class La classe en question.
    /**
     * @param string $id
     * @param string $class
     * @return $this
     */
    public function addAlias(string $id, string $class ): self
    {
        $this->aliases[$id] = $class;

        return $this;
    }
 }